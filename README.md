# Example for multi-process CI job
The included CI job starts an HTTP server, performs HTTP client requests, and then shuts down the server.

## Job script
```bash
pip install flask
python -m flask --app server run --port 50052 > server.log 2>&1 & server_pid=$!
sleep 1
python client.py
kill -INT $server_pid
```

#### Explanation
- `pip install flask`: install server requirements
- `flask --app server run --port 50052`: run the HTTP server defined in `server.py` on the local port 50052
    - `> server.log`: redirect standard output to the file `server.log`
    - `2>&1`: merge error output to standard output
    - `&`: detach process to run in background
    - `server_pid=$1`: `$1` is the process ID ("PID") of the detached process, which is stored in the variable `server_pid`
- `sleep 1`: wait a second to accomodate for server startup time
- `python client.py`: run the HTTP client script which interacts with the server running in the background
- `kill -INT $server_pid`: send the interrupt signal ("SIGINT") to the server process to shut it down (equivalent to Ctrl+C)

## CI definition
The CI job is defined in `.gitlab-ci.yml`:
```yaml
run-server-client:
  image: python:alpine
  script:
    - pip install flask
    - python -m flask --app server run --port 50052 > server.log 2>&1 & server_pid=$!
    - sleep 5
    - python client.py
    - kill -INT $server_pid
  artifacts:
    paths:
      - server.log
```

#### Explanation
- `run-server-client`: job name (can be anything)
- `image: python:alpine`: Docker image to use for this job
- `script`: commands to be executed for the job
- `artifacts`: files to be exported after job execution

## Artifacts
The CI job `run-server-client` writes all server output to `server.log` and exports this file as artifact. It can be downloaded via this url ([documentation](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#access-the-latest-job-artifacts-by-url)):

https://gitlab.com/nmertsch/multiprocess-ci-example/-/jobs/artifacts/main/raw/server.log?job=run-server-client
