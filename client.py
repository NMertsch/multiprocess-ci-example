from http.client import HTTPConnection

con = HTTPConnection("localhost:50052")

def request(message: str) -> str:
    con.request("GET", f"/echo/{message}")
    return con.getresponse().read().decode("UTF-8")

for message in ["hello", "world"]:
    print(request(message))
